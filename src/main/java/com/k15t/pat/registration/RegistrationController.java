package com.k15t.pat.registration;

import java.io.StringWriter;

import javax.ws.rs.core.Response;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.k15t.pat.registration.entity.User;

@RestController
public class RegistrationController {

	@Autowired
	private VelocityEngine velocityEngine;

	@Autowired
	private RegistrationResource registrationResource;

	@RequestMapping("/registration.html")
	public String registration() {

		Template template = velocityEngine.getTemplate("templates/registration.vm");
		VelocityContext context = new VelocityContext();
		StringWriter writer = new StringWriter();
		context.put("records", registrationResource.findAll());
		template.merge(context, writer);

		return writer.toString();
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String save(@ModelAttribute User user) {
		Response resp = registrationResource.save(user);

		System.out.print(resp.getStatusInfo());

		Template template = velocityEngine.getTemplate("templates/registration.vm");
		VelocityContext context = new VelocityContext();
		StringWriter writer = new StringWriter();
		context.put("records", registrationResource.findAll());
		template.merge(context, writer);

		return writer.toString();
	}

	@RequestMapping(value = "/delete/{name}", method = RequestMethod.GET)
	public String delete(@PathVariable("name") String name) {
		Response resp = registrationResource.delete(name);

		System.out.print(resp.getStatusInfo());

		Template template = velocityEngine.getTemplate("templates/registration.vm");
		VelocityContext context = new VelocityContext();
		StringWriter writer = new StringWriter();
		context.put("records", registrationResource.findAll());
		template.merge(context, writer);

		return writer.toString();
	}
}
