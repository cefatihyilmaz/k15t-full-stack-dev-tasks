package com.k15t.pat.registration;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.k15t.pat.registration.entity.User;

@Path("/registration")
@Component
public class RegistrationResource {

	// Extend the current resource to receive and store the data in memory.
	// Return a success information to the user including the entered information.
	// In case of the address split the information into a better format/structure
	// for better handling later on.

	List<User> userList = new ArrayList<>();

	public List<User> findAll() {
		return userList;
	}

	public Response save(User user) {
		userList.add(user);

		return Response.ok().build();
	}

	public Response delete(String name) {
		List<User> toBeDeleted = userList.stream().filter(usr -> usr.getName().equals(name))
				.collect(Collectors.toList());

		userList.removeAll(toBeDeleted);
		return Response.ok().build();
	}

}
